/* generator.c
 *
 * Copyright 2018 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generator.h"

static gchar *
extract_initials_from_name (const gchar *name)
{
        GString *initials;
        g_autofree gchar *p = NULL;
        g_autofree gchar *normalized = NULL;
        gunichar unichar;
        gpointer q = NULL;

        p = g_utf8_strup (name, -1);
        normalized = g_utf8_normalize (g_strstrip (p), -1, G_NORMALIZE_DEFAULT_COMPOSE);
        if (normalized == NULL) {
                return NULL;
        }

        initials = g_string_new ("");

        unichar = g_utf8_get_char (normalized);
        g_string_append_unichar (initials, unichar);

        q = g_utf8_strrchr (normalized, -1, ' ');
        if (q != NULL && g_utf8_next_char (q) != NULL) {
                q = g_utf8_next_char (q);

                unichar = g_utf8_get_char (q);
                g_string_append_unichar (initials, unichar);
        }

        return g_string_free (initials, FALSE);
}

static GdkRGBA
get_color_for_name (const gchar *name)
{
        // https://gitlab.gnome.org/Community/Design/HIG-app-icons/blob/master/GNOME%20HIG.gpl
        static gdouble gnome_color_palette[][3] = {
                {  98, 160, 234 },
                {  53, 132, 228 },
                {  28, 113, 216 },
                {  26,  95, 180 },
                {  87, 227, 137 },
                {  51, 209, 122 },
                {  46, 194, 126 },
                {  38, 162, 105 },
                { 248, 228,  92 },
                { 246, 211,  45 },
                { 245, 194,  17 },
                { 229, 165,  10 },
                { 255, 163,  72 },
                { 255, 120,   0 },
                { 230,  97,   0 },
                { 198,  70,   0 },
                { 237,  51,  59 },
                { 224,  27,  36 },
                { 192,  28,  40 },
                { 165,  29,  45 },
                { 192,  97, 203 },
                { 163,  71, 186 },
                { 129,  61, 156 },
                {  97,  53, 131 },
                { 181, 131,  90 },
                { 152, 106,  68 },
                { 134,  94,  60 },
                {  99,  69,  44 }
        };

        GdkRGBA color = { 255, 255, 255, 1.0 };
        guint hash;
        gint number_of_colors;
        gint idx;

        if (name == NULL || strlen (name) == 0)
                return color;

        hash = g_str_hash (name);
        number_of_colors = G_N_ELEMENTS (gnome_color_palette);
        idx = hash % number_of_colors;

        color.red   = gnome_color_palette[idx][0];
        color.green = gnome_color_palette[idx][1];
        color.blue  = gnome_color_palette[idx][2];

        return color;
}

cairo_surface_t *
generate_user_picture (const gchar *name, gint size)
{
        PangoFontDescription *font_desc;
        g_autofree gchar *initials = extract_initials_from_name (name);
        g_autofree gchar *font = g_strdup_printf ("Sans %d", (int)ceil (size / 2.5));
        PangoLayout *layout;
        GdkRGBA color = get_color_for_name (name);
        cairo_surface_t *surface;
        gint width, height;
        cairo_t *cr;

        surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
                                              size,
                                              size);
        cr = cairo_create (surface);
        /* Clip a circle */
        cairo_arc (cr, size/2, size/2, size/2, 0, 2 * G_PI);
        cairo_clip (cr);
        cairo_new_path (cr);
        cairo_set_source_rgb (cr, color.red/255.0, color.green/255.0, color.blue/255.0);
        cairo_paint (cr);
        cairo_fill (cr);

        /* Draw the initials on top */
        cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
        layout = pango_cairo_create_layout (cr);
        pango_layout_set_text (layout, initials, -1);
        font_desc = pango_font_description_from_string (font);
        pango_layout_set_font_description (layout, font_desc);
        pango_font_description_free (font_desc);

        pango_layout_get_size (layout, &width, &height);
        cairo_translate (cr, size/2, size/2);
        cairo_move_to (cr, - ((double)width / PANGO_SCALE)/2, - ((double)height/PANGO_SCALE)/2);
        pango_cairo_show_layout (cr, layout);
        cairo_destroy (cr);

        return surface;
}
