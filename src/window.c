/* window.c
 *
 * Copyright 2018 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window.h"
#include "generator.h"

struct _UserImageGeneratorWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkImage            *avatar;
  GtkEntry            *entry;
};

#define IMAGE_SIZE 512

G_DEFINE_TYPE (UserImageGeneratorWindow, user_image_generator_window, GTK_TYPE_APPLICATION_WINDOW)

static void
on_entry_changed (GtkEntry *entry, gpointer  user_data) {
  UserImageGeneratorWindow *self = user_data;
  cairo_surface_t *surface;
  const gchar *name;
  
  name = gtk_entry_get_text (entry);
  surface = generate_user_picture (name, IMAGE_SIZE);
  
  gtk_image_set_from_surface (self->avatar, surface);
}

static void
user_image_generator_window_class_init (UserImageGeneratorWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/UserImageGenerator/window.ui");
  gtk_widget_class_bind_template_child (widget_class, UserImageGeneratorWindow, avatar);
  gtk_widget_class_bind_template_child (widget_class, UserImageGeneratorWindow, entry);
  
  gtk_widget_class_bind_template_callback (widget_class, on_entry_changed);
}

static void
user_image_generator_window_init (UserImageGeneratorWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  
  on_entry_changed (self->entry, self);
}
